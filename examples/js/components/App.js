/* eslint max-len: 0 */
import React from 'react';
import { LinkContainer } from 'react-router-bootstrap';

// import 'bootstrap/dist/css/bootstrap.css';
import 'toastr/build/toastr.min.css';
import '../../../css/react-bootstrap-table.css';
// import 'jquery';
// import 'bootstrap';
import {
  Navbar,
  Nav,
  NavItem,
  NavDropdown,
  MenuItem,
  Grid,
  Row,
  Col
} from 'react-bootstrap';

class App extends React.Component {

  static propTypes = {
    children: React.PropTypes.node
  };

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const examples = [ {
      text: 'React JS',
      href: 'manual'
    }, {
      text: 'Integrantes',
      href: 'integrantes'
    } ];

    const exampleMenuItems = examples.map((item, idx) => {
      return (
        <LinkContainer key={ idx } to={ '/examples/' + item.href }>
          <MenuItem key={ idx }>{ item.text }</MenuItem>
        </LinkContainer>
      );
    });
    return (
      <div>
        <Navbar inverse toggleNavKey={ 0 }>
          <Nav>
            <LinkContainer to='/examples/complex'>
              <NavItem>CRUD</NavItem>
            </LinkContainer>
            <NavDropdown title='Documentación' id='collapsible-navbar-dropdown'>
              { exampleMenuItems }
            </NavDropdown>
          </Nav>
        </Navbar>
        <Grid fluid>
          <Row>
            <Col md={ 12 }>
              { this.props.children }
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default App;
