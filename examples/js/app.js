import React from 'react';
import ReactDOM from 'react-dom';

import { Router, Route } from 'react-router';
import createHistory from 'history/lib/createHashHistory';

const history = createHistory( { queryKey: false } );

import App from './components/App';
import PageNotFound from './components/PageNotFound';


const routes = (
  <Router history={ history }>
    <Route path='/' component={ App }>
      <Route path='examples'>
        <Route path='complex' component={ require('./complex/demo') } />
        <Route path='manual' component={ require('./manual/manual') } />
        <Route path='integrantes' component={ require('./integrantes/integrantes') } />
      </Route>
      <Route path='*' component={ PageNotFound }/>
    </Route>
  </Router>
);

ReactDOM.render(routes, document.querySelector('#root'));
