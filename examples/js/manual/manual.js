import React from 'react';

class Manual extends React.Component {
  render() {
    return (
      <div className='col-md-offset-1 col-md-10'>
        <div className='panel panel-default'>
            <div className='panel-heading'><strong>Qué es React JS</strong></div>
            <div className='panel-body'>
                <p>Lo que ha hecho que React se vuelva trending es su velocidad de renderizado de vistas. Eso es posible gracias a un Virtual DOM que genera React con cada componente que creamos y el algoritmo de Diff que básicamente lo que hace es marcar que elementos dentro de nuestro DOM Virtual tienen cambios para renderizar solo ellos y no tener que revisar y repintar el DOM entero de nuestra página. Dónde más tiempo se pierde en una aplicación web es en el renderizado y pintado del DOM. React evita eso y por eso es tan rápido.</p>
            </div>
          <div className='panel-heading'><strong>Virtual DOM</strong></div>
          <div className='panel-body'>
              <p>Lo que ha hecho que React se vuelva trending es su velocidad de renderizado de vistas. Eso es posible gracias a un Virtual DOM que genera React con cada componente que creamos y el algoritmo de Diff que básicamente lo que hace es marcar que elementos dentro de nuestro DOM Virtual tienen cambios para renderizar solo ellos y no tener que revisar y repintar el DOM entero de nuestra página. Dónde más tiempo se pierde en una aplicación web es en el renderizado y pintado del DOM. React evita eso y por eso es tan rápido.</p>
          </div>
          <div className='panel-heading'><strong>Diseño orientado a componentes</strong></div>
          <div className='panel-body'>
              <p>React nos obliga a pensar en componentes. Es la nueva tendencia en el mundo del desarrollo Frontend. Al igual que en el backend se tiende a usar micro-servicios y librerías que resuelvan una cosa concreta, en el Frontend cada vez más se está extendiendo ésta práctica de componetizar los elementos de nuestras aplicaciones para poderlos reutilizar.</p>
          </div>
          <div className='panel-heading'><strong>Dependencias necesarias para ejecutar React JS</strong></div>
          <div className='panel-body'>
              <ul>
                  <li>
                      Se necesita tener instalado Node js.
                      <a href='https://nodejs.org/en/' alt='link-node'> Descargar </a>
                  </li>
                  <li>
                      Clonar o descargar el siguiente proyecto de Bitbucket.
                      <a href='https://github.com/AllenFang/react-bootstrap-table' alt='link-node'> Descargar </a>
                  </li>
                  <li>
                      Clonar o descargar el siguiente proyecto de Bitbucket.
                      <a href='https://github.com/AllenFang/react-bootstrap-table' alt='link-node'> Descargar </a>
                  </li>
              </ul>
          </div>
        </div>
      </div>


    );
  }
}

export default Manual;
