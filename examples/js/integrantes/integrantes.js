import React from 'react';

class Integrantes extends React.Component {
  render() {
    return (
      <div className='col-md-offset-1 col-md-10'>
        <div className='panel panel-default'>
          <div className='panel-heading'>Integrantes</div>
          <div className='panel-body'>
              <ul>
                  <li>Bautista Carina</li>
                  <li>Benavides Andrés</li>
                  <li>Calderón Diego</li>
                  <li>Yandún Mauricio</li>
              </ul>
          </div>
        </div>
      </div>

    );
  }
}

export default Integrantes;
